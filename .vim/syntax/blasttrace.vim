" BLAST trace syntax highlighting
" Introduces hotkeys and colors for BLAST debug traces
" 
" Written by Pavel Shved <shved@ispras.ru>
" GPLv3, NO WARRANTY 
"

" Result of post
syntax match ResultRegion /^RGN>/
hi ResultRegion ctermfg=green

" Outlight each block when building trace preds
syntax match TraceBlockHeader /cons_folder !posn =.*/
syntax match TraceBlockHeader /^pos =.*/
hi TraceBlockHeader ctermfg=yellow

syntax match TraceBlockSubHeader /^Now asserting block.*/
hi TraceBlockSubHeader ctermfg=Brown

"Simplify detects contradiciton
syntax match SimplifyValid /^Simplify says.*Valid/
hi SimplifyValid ctermfg=red

"Conflicting blocks
"syntax match ConflictingBlocks /^Conflicting Blocks/
syntax match ConflictingBlocks /^\[INF.*/
hi ConflictingBlocks ctermfg=magenta

syntax match ConflictingBlocks /CSIsat yields b_Interpolant: .*/hs=s+29

