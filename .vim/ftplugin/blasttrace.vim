" BLAST trace filetype mappings
" Introduces hotkeys and colors for BLAST debug traces
" 
" Written by Pavel Shved <shved@ispras.ru>
" GPLv3, NO WARRANTY 
"

if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

" Next iteration
nmap <buffer> <C-N> :exec "/^\\*\\+$"<CR>zzj
nmap <buffer> <C-P> k:exec "?^\\*\\+$"<CR>zzj
"	Note: j and k are needed to allow easier traversing in error node.  There we can find previous string full of *s without bothering whether we're on such a string already
vmap <buffer> <C-N> :exec "/^\\*\\+$"<CR>j
vmap <buffer> <C-P> k:exec "?^\\*\\+$"<CR>j

" Next errorneous node
nmap <buffer> <C-E> <C-N>:exec "/Error found : checking validity"<CR><C-P>
nmap <buffer> <C-R> :exec "?Error found : checking validity"<CR><C-P>

" Generic traversing
nmap <buffer> <CR> /^\*\+$\\|^RGN> \nIn\\|Error found : check\\|Starting phase 2\\|THE CONSTRAINTS ARE\\|Conflicting Blocks\\|Let's refine the nodes along that path\\|b_Interpo\\|Leaf Nodes\\|Remaining nodes\\|This is a false error<CR>zz
nmap <buffer> <BS> ?^\*\+$\\|^RGN> \nIn\\|Error found : check\\|Starting phase 2\\|THE CONSTRAINTS ARE\\|Conflicting Blocks\\|Let's refine the nodes along that path\\|b_Interpo\\|Leaf Nodes\\|Remaining nodes\\|This is a false error<CR>zz

""Error node traversing
" helper: jump to the beginning of this node
nmap <buffer> <B :exec "?^\\*\\+$"<CR>

" Start trace analysis
nmap <buffer> ,t <B:exec "/Starting phase 2"<CR>z<CR>

" Constraints of blocks
nmap <buffer> ,y <B:exec "/THE CONSTRAINTS ARE"<CR>z<CR>

" Conflicting blocks
nmap <buffer> ,u :exec "/Conflicting Blocks"<CR>zz
nmap <buffer> <U :exec "?Conflicting Blocks"<CR>zz

" Interpolant
nmap <buffer> ,i :exec "/b_Interpo"<CR>zz
nmap <buffer> <I :exec "?b_Interpo"<CR>zz

" Easy find the end of the error trace
nmap <buffer> ,e G:exec "?Call.*blast_assert"<CR>

" Leaf nodes
nmap <buffer> ,o :exec "/Leaf Nodes"<CR>z<CR>
nmap <buffer> <o :exec "?Leaf Nodes"<CR>z<CR>

" Remaining nodes
nmap <buffer> ,p <B:exec "/Remaining nodes:"<CR>z<CR>


