" Vim indent file
" Language: C++
" Shved Pavel

if exists("b:did_indent")
	finish
endif
let b:did_indent = 1
" set cinoptions=l1,b1,g1s,h0,i1s,+2s,c2s,(0,W1s,m1
" set cinkeys-=0#
" setlocal cindent
" finish



setlocal indentexpr=CPP_Patch()
if exists("*CPP_Patch")
	finish
endif

" Here's a little patch for my own style of formatting set of options
" If previous line starts with a comma, make next line indented the same
function CPP_Patch()
	let pLine = getline(v:lnum)
	let pLineNum = prevnonblank(v:lnum)
	if pLineNum == 0
		return 0
	endif
	let cLine = getline(v:lnum+1)

	let pLine = getline(pLineNum)
	let pLineInd = indent(pLineNum)
	if pLine =~ '^\s*,.*' "&& !(cLine =~'^\s*.?)')
		return pLineInd
	else
		" then we do casual C indenting
		let b:did_indent = 1
		return cindent(v:lnum)
	endif
endfunction



