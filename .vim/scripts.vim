" BLAST trace filetype detection script
" 
" Written by Pavel Shved <shved@ispras.ru>
" GPLv3, NO WARRANTY 
"

if did_filetype()	" filetype already set..
	finish		" ..don't do these checks
endif

if getline(1) =~ '^BLAST' && getline(2) =~ '^Blast was run with the following command line'
	setfiletype blasttrace
endif

