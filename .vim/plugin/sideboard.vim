" sideboard.vim
" ===============
" by Shved Pavel
"
"	Pick up lines and append them to text file with a couple of buttons
" Last Change: 5 Aug 2009
" Maintainer: Shved Pavel <pavel.shved@gmail.com>
"

let s:save_cpo = &cpo
set cpo&vim

if exists('Sideboard_sourced') || &cp
	finish
endif
let Sideboard_sourced=1

" Global variables
let g:Sideboard_file = ''
	"File to append changes to

" Commands and keys mappings
"set verbose=9

command -nargs=1 -complete=file Side call Sideboard_load(<f-args>,0)
command -nargs=1 -complete=file SideX call Sideboard_load(<f-args>,1)
command Show call Sideboard_show()
nmap <silent> <C-A> :call Sideboard_yank_n()<CR>
vmap <silent> <C-A> :call Sideboard_yank_v()<CR>

" Functions' definitions
function! Sideboard_load(fileName,clear)
	" Clear file first
	if a:clear == 1
		call system("rm ".a:fileName)
		call system("touch ".a:fileName)
	endif
	let g:Sideboard_file = a:fileName
endfunction

function! Sideboard_yank_n()
	if g:Sideboard_file == '' 
		echoerr "You can't cat to sideboard unless you've specified one with Side!" 
		return -1
	endif

	let line = getline(".")
	call system("echo '".line."' >> ".g:Sideboard_file)
endfunction

function! Sideboard_yank_v()
	if g:Sideboard_file == '' 
		echoerr "You can't cat to sideboard unless you've specified one with Side!" 
		return -1
	endif

	exe '"hy'
	call system("echo '".getreg('h')."' >> ".g:Sideboard_file)
endfunction

function! Sideboard_show()
	if g:Sideboard_file == '' 
		echoerr "You can't show your sideboard unless you've specified one with Side!" 
		return -1
	endif

	exe "!cat ".g:Sideboard_file

endfunction

let &cpo = s:save_cpo
