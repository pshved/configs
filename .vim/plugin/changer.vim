" changer.vim
" ===============
" by Shved Pavel
"
" Plugin that maintains file-list to quickly jump over files in a particular 
" project. Any text file (and even its part!) that contains filenames and
" directoy structure can be a filelist. It will be used mainly to cause previous
" active window to edit the file under selection.
" Last Change: 18 Mar 2008
" Maintainer: Shved Pavel <pavel.shved@gmail.com>
"
" Community Notes:
" I realy don't understand why i should write in caps that there comes 
" NO WARRANTY with this script, which is only a chunk of letters that luckily 
" do something funny. If you kill a man or start a nuclear war with it, mail me
" for me to have some fun. Besides, in no official document was it promised for
" this script or me to do or not to do anything, thus nothing there is for you 
" to complain to or require.
"
" If something that concerns this script had dealt some damage to you or 
" someone else the best thing you can do is letting me know what it was and how
" to fix it (don't mail patches to me because i'd love to write all code by 
" myself). Don't let others to share your experience!
"
" One little thing: if you are writing something with its functionality being 
" similar to Changer and use this script as a guide or a source of text to copy
" and paste, please, write a note about it. Thank you.
"
" Some Help:
" I don't wanna write a help file: that's too late and i gonna give up its 
" developement for a while. Here comes all help for it:
"
" INVOKING CHANGER:
"
" To invoke Changer, :call Changer_Load <flie-list>. 
" <file-list> can be any textfile. The way of formatting it will be discussed later.
" A good idea is to place this call to startup-script. Later a built-in view-saver will be implemented.
"
" HOTKEYS:
" After its invocation you can use <F12> to jump directly to its window. Move cursor to select the file you need and press <CR> (Enter) to edit it in the window you've been before it. Or press <C-X> to open it in the way your shell does. Or press <C-E> to tee its output to previous window (if you want, you may specify command-line arguments directly in file-list, append it after a whitespace. It won't interfere <CR> action). This teeing uses temporary file, so you must have write permission to dir, where file-list resides.
" Try <F11> to switch to prewiew mode, where new files open as you move cursor in the file-list.
" But PLEASE, do NOT edit the file-list by opening from itself!!! You already have a window with it, WHY do you need a second one?
"
" FILELIST FORMAT:
" First you should think about list of filenames. A path of directory where file-list resides will be appended to these filenames as you open these files.
" Your second thought is about directories that contain even more files. That's done like this: you start with directory name, you do not forget a '/' after it and then you make an additional indent on the line after it where you write a filename.
" An example:
">file_in_dir_where_file-list_resides
">dir/
">	file_in_dir
">	subdir/
">		file_in_subdir
">	another_file_in_dir
">file_not_in_dir_anymore
">file
">	_which_has_a_common_part_with_others
">	_that_starts_with_same_word_as_previous_one
"
" Yes, you are right: you may not only specify a directory, but any leading common part in filnames. (Trailing command parts' format is under development)
" The best way to obtain a list of files in a subdir is to press <CR> on it. netrw plugin should appear in the previous window and you can now yank and paste to file-list. You are a good boy and mommy taught you how to paste and reindent massively?
"
" All paths are relative to the directory where file-list resides. If you want an absolute path, start it with '/'. You can still use common-parts-technique described above.
" More tricky: Changer inserts file-list path with trailing '/' in the beginning of any filename compiled from within a file-list. But if resultant path looks like that: 'foo//bar' (with two backslashes), it cuts 'foo/' so only '/bar' remains, which is an absolute path you were so starving to obtain.
"
" You can specify comment lines. If line starts with a = it's treated as empty (that's a comment). If line starts with ! it's treated as not comtaining any name BUT its indent is accounted in path construction as entity with indent and no string. Example:
">name
">=comment
">	_shift
">!section
">	_shift
"
" Pressing <CR> on the third line will result in editing name_shift, but pressing it at 5th line will result in editing _shift instead.
"
" You may want to correct highlighting. Refer to Changer_Load() function then.
"
" That's all help available for now. If you need more help, consult the sript itself. It's not complex, though my notation of function naming is not so common.
"
" The script is NEITHER FOOLPROOF NOR INTENDED TO USE IN AN UNCOMMON WAY. Fools and preverts should use something else. Or wait for me or someone to re-write this script for it to statisfy THEIR needs.
" The other side of using it in an uncommon way is discovering new features - I tried to make script as simple and natural as it possible. Hence it can exhibit some features under uncommon curcumstances or usage. Sorry for 'preverts' in previous paragraph.
" 
"
" Todo:
" 	+ Add folders
" 	+	Add relative paths, when filelist from a different folder is loaded.
" 	+ Add absolute paths
" 	+ Make jumping by <F12> to the name of the file that's currently editing
" 	+ Make folders' expanding (i.e. creating a list of files that are there).
" 		+ or force user to copy it from netrw?
" 	+	Make selection highlighting
" 	+ Make preview mode
"		+ Tweak tabstop and shiftwidth in List window
" 	+ Store window-id and add quickjump to list
" 	+ Make syntax highlighting
" 	- Make auto-indenting
" 	+ Add 'shell execute' 
" 	+ Add 'tee to last window'
" 	N	Make file 'nicknames'
" 	N Make auto-adjusting the width of filelist
" 	N Add shortcuts to switch between short and wide views of list
" 	+	Use folding capabilities
" 		+ write a note in help that user must use indent-folding
" 	+ Add shortcuts
" 		+ <F12> to switch between filelist and editor
" 		+ <CR> to open file
" 		+ <F11> to scroll through files
" 		+ <F11> to jump to list window in preview mode


let s:save_cpo = &cpo
set cpo&vim

if exists('Changer_sourced') || &cp
	finish
endif
let Changer_sourced=1

" Global variables
let g:Changer_loaded = 0
	"Whether Changer is loaded, has a buffer associated with him and all variables are set
let g:Changer_listBufferID = -1
	"Number of a buffer where filelist is loaded into
let g:Changer_ViewMode = -1
	"View mode. Can be 1(normal) or 2(preview), or 3(none)
let g:Changer_CursorLnum = -1
let g:Changer_CursorCol = -1
	"Cursor information of abandoned list window
let g:Changer_autoCmdNo=[0,0]
	"Disabling autocommands
	" 0 - all, 1 - switching selection

" Commands and keys mappings
"set verbose=9

command -nargs=1 -complete=file Changer call Changer_Load(<f-args>)
nmap <silent> <F12> :call Changer_WindowSwitch()<CR>
nmap <silent> <F11> :call Changer_WindowSwitch()<CR>:call Changer_ViewModeSet(2)<CR>:call Changer_CursorMoved()<CR>
augroup Changer_Selection
	au!
	autocmd! BufEnter * call Changer_SelectionSet_InTargetBuffer()
augroup END


" Functions' definitions
function! Changer_Load(listFileName)
	if !exists('g:Changer_loaded') || g:Changer_loaded!=1 
		" Loading buffer
		" For now we load a file in a window instead of buffer
		topleft 20vne
		exe 'edit '.a:listFileName
		" Obtain a buffer recently loaded
		let s:Changer_listBufferID = bufnr(a:listFileName)
		"echomsg s:Changer_listBufferID

		" Buffer mappings and settings
		noremap <buffer> <silent> <C-X> :call Changer_ShellExecute()<CR>
		noremap <buffer> <silent> <C-E> :call Changer_TeeExecute()<CR>
		noremap <buffer> <silent> <CR> :call Changer_FileAtCursorOpen(0)<CR>
		noremap <buffer> <silent> <F11> :call Changer_ViewModePreviewToggle()<CR>:call Changer_CursorMoved()<CR>
		autocmd! BufLeave <buffer> call Changer_ListWindowOnLeave()
		autocmd! CursorMoved <buffer> call Changer_CursorMoved()
		call Changer_ViewModeSet(1)
		
		" Some folders maintenance
		let sw=&shiftwidth
		if sw>4
			"Too big shiftwidth will take too much space
			let sw = 4
		endif
		exe 'setlocal tabstop='.sw
		setlocal noexpandtab

		"defining syntax
		highlight Changer_Folder ctermfg=Cyan
		syntax match Changer_Folder /\v\S+\//

		highlight Changer_Comment ctermfg=Gray
		syntax match Changer_Comment /\v^\s*!.*$/

		"here comes different matches. I am new to programming, so the list will be more populated... later :-)
		"matches
		"they start with ^ as we encounter filenames only in the beginning of lines. The rest is arguments.
		syntax match Changer_Code /\v^\s*\S+(\.cpp|\.c|\.hpp|\.h|\.pl)/
		syntax match Changer_Script /\v^\s*(\S+(\.sh|\.bat|\.sql))/
		syntax match Changer_Markup /\v^\s*\S+(\.html|\.htm|\.xml|\.xslt|\.xhtml)/
		syntax match Changer_Text /\v^\s*(\S+\.txt)/
		syntax match Changer_Maintenance /\v^\s*(README.*|INSTALL.*|Makefile|makefile|configure.*|Makefile\.PL)/
		"and highlightings
		highlight Changer_Code ctermfg=LightGreen
		highlight Changer_Script ctermfg=Magenta
		highlight Changer_Markup ctermfg=DarkRed
		highlight Changer_Text ctermfg=LightGray
		highlight Changer_Maintenance ctermfg=DarkCyan

		"Folds
		setlocal foldmethod=indent
		norm zR
		setlocal foldlevel=3

		"wordwrap
		setlocal nowrap

		" Success
		let g:Changer_loaded=1
	endif
endfunction

function! Changer_WindowSwitch()
	if !exists('g:Changer_loaded') || g:Changer_loaded!=1 "|| buffer doesnt exist
		echoerr "You can't switch to Changer window if it's not loaded!" 
		return -1
	endif

	if s:Changer_listBufferID == -1 || bufwinnr(s:Changer_listBufferID) == -1
		return "Window to switch to is not found."
		return -1
	endif

	if winbufnr(0)==s:Changer_listBufferID
		"switch to previous window
		wincmd p
	else
		let winn = bufwinnr(s:Changer_listBufferID)
		exe winn."wincmd w"
		"set Normal mode
		call Changer_ViewModeSet(1)
	endif
endfunction

function! Changer_ListWindowOnLeave()
	let g:Changer_CursorLnum = line(".")
	let g:Changer_CursorCol = col (".")
	Changer_ViewModeSet(1)
endfunction

function! Changer_ViewModePreviewToggle()
	if (g:Changer_ViewMode>=0)
		if g:Changer_ViewMode==1
			call Changer_ViewModeSet(2)
		else
			call Changer_ViewModeSet(1)
		endif
	endif
	return g:Changer_ViewMode
endfunction

function! Changer_ViewModeSet(mode)
	if a:mode==1
		set cursorline
		highlight CursorLine cterm=bold ctermfg=black ctermbg=yellow
	endif
	if a:mode==2
		set cursorline
		highlight CursorLine cterm=bold ctermfg=black ctermbg=lightgreen
	endif
	if a:mode==3
		set nocursorline
	endif
	let g:Changer_ViewMode = a:mode
	return a:mode
endfunction

function! Changer_ViewModeGet()
	return g:Changer_ViewMode
endfunction

function! Changer_CursorMoved()
	if g:Changer_ViewMode!=2
		return -1
	endif
	call Changer_FileAtCursorOpen(1)
endfunction

function! Changer_SelectionSet_InTargetBuffer()
	if !exists('g:Changer_loaded') || g:Changer_loaded!=1 "|| buffer doesnt exist
		return -1
	endif

	if s:Changer_listBufferID == -1 || bufwinnr(s:Changer_listBufferID) == -1
		return -1
	endif

	if g:Changer_autoCmdNo[0] || g:Changer_autoCmdNo[1]
		return -1
	endif

	if winbufnr(0)==s:Changer_listBufferID
		"we've enterred the list window
		"restore cursor position
		"call cursor(g:Changer_CursorLnum,g:Changer_CursorCol)
		call Changer_ViewModeSet(1)
	else
		"We are not in list window so we can do selecting
		"But first we save window-switching history
		"we obtain window changing history
		let winCurrentNum = winnr()
		let winPrevNum = winnr("#")
		let fileName = bufname("%")
		"switch to list
		let winn = bufwinnr(s:Changer_listBufferID)
		exe winn."wincmd w"
		"Change selection
		let lineNum=Changer_FullNameFind(fileName)
		if lineNum == -1
			call cursor(g:Changer_CursorLnum,g:Changer_CursorCol)
			call Changer_ViewModeSet(3)
		else
			call cursor(lineNum,1)
			call Changer_ViewModeSet(1)
		endif
		" Return back and restore
		exe winPrevNum."wincmd w"
		exe winCurrentNum."wincmd w"
	endif
endfunction

function! Changer_FileCreate(fileName)
	if !strlen(a:fileName)
		return -1
	endif
	if a:fileName[strlen(a:fileName)-1]=='/'
		"create a diectory (with parents)
		call system("mkdir -p ".a:fileName)
	else
		"when we create a file, we should be sure whether directory he's in exists
		"so, create it!
		call system("mkdir -p ".Changer_PathExtract(a:fileName))
		"vim can create a file in a directory, so it's enough for now
	endif
endfunction

function! Changer_PathExtract(fileName)
"Extracts path from fileName string
	let splitted=matchlist(a:fileName,'\v^(/=.*/)')
	if len(splitted)>=2 && splitted[1]!=""
		return splitted[1]
	else
		return ""
	endif

endfunction

function! Changer_Significant(line,task)
"Checks whether line is significant for at least one task in list a:task
	if index(a:task,"path")!=-1 || index(a:task,"exec")!=-1 
		"Check for being a comment
		if match(a:line,'\v^\s*\=') != -1
			return 0
		endif
		"Check for filename existence
		let splitted=matchlist(a:line,'\v^\s*(\S+)')
		return len(splitted)>=2 && splitted[1]!=""
	endif
endfunction

function! Changer_FullNameFind(nameTarget)
"Searches for a full file name in current buffer
"Return: -1 if not found, line number if found
	let nameCurr=Changer_PathExtract(bufname("%"))
		"current file name
	let namesStack=[[-1,nameCurr]]
		"stack of records: [indent,name]
	let lineNum=0
	let lineLastNum=line('$')
	while namesStack[-1][1]!=a:nameTarget 
		"increase number and test whether we're after EOF
		let lineNum = lineNum + 1
		if lineNum>lineLastNum
			break
		endif
		if !Changer_Significant(getline(lineNum),["path"])
			continue
		endif

		let indentCurr = indent(lineNum)

		"Processing stack -- no recursion!
		let topNewIndex = len(namesStack)
		while indentCurr<=namesStack[topNewIndex-1][0]
			"We should not check for list to be non-empty as it always contains -1 
			"in the beginning so the loop will be breaking before we pop whole stack
			let topNewIndex = topNewIndex - 1
		endwhile
		if topNewIndex<len(namesStack)
			call remove(namesStack,topNewIndex,len(namesStack)-1)
		endif
		"Now we add current item to the stack
		call add(namesStack,[indentCurr,namesStack[len(namesStack)-1][1].Changer_FileNameGet(getline(lineNum))])
	endwhile
	if lineNum<=0 || lineNum>lineLastNum
		let lineNum = -1
	endif
	return lineNum
endfunction

function! Changer_FullNameGet_FromFlist(lineNum)
	let lineNum=a:lineNum
	if !Changer_Significant(getline(lineNum),["path"])
		return ""
	endif
	let indentPrev=indent(lineNum)
	let fileName = Changer_FileNameGet(getline(lineNum))
	
	"Walking upward until we encounter start of file or realize that we cant 
	"unindent anymore
	let lineNum = lineNum - 1
	while (indentPrev>0 && lineNum>0)
		if Changer_Significant(getline(lineNum),["path"]) && indent(lineNum)<indentPrev
			let fileName = Changer_FileNameGet(getline(lineNum)).fileName
			let indentPrev = indent(lineNum)
		endif
		let lineNum = lineNum - 1
	endwhile
	
	return fileName
endfunction

function! Changer_FullNameGet(lineNum)
"gets the 'full' path of a file in line lineNum at buffer bufferID.	
"Note: the script is not recursive although it starves for it due to Vim's 
"	recursion-depth constraits.
"We 
	
	"And finally we append fileName to the directory where current script resides
	return Changer_PathExtract(bufname("%")).Changer_FullNameGet_FromFlist(a:lineNum)
endfunction

function! Changer_ArgsGet(fromLine)
	"we match filename as first WORD in the line we're at
	let splitted=matchlist(a:fromLine,'\v^\s*\S+\s+(.*)')
	if len(splitted)<2
		return ""
	endif
	return splitted[1]
		"we can return an empty string also
endfunction

function! Changer_FileNameGet(fromLine)
	"First we check line for being a comment
	if match(a:fromLine,'\v^\s*!') != -1
		return ""
	endif
	"we match filename as first WORD in the line we're at
	let splitted=matchlist(a:fromLine,'\v^\s*(\S+)')
	return splitted[1]
		"we can return an empty string also
endfunction

function! Changer_FileAtCursorOpen(dontLeaveBuffer)
	if !Changer_Significant(line('.'),["path"])
		echomsg "Empty line, doing nothing."
		return -1
	endif
	"We must obtain it before we leave current buffer
	let fileName = Changer_FullNameGet(line('.'))
	if !strlen(fileName)
		echomsg "Empty line, doing nothing."
		return -1
	endif
	let fileName=Changer_Absolutize(fileName)

	"Finally we check whether entity (file or dir) exists
	if g:Changer_ViewMode==2
		if !strlen(getftype(fileName))
			echomsg "Such file (".fileName.") does not exist!"
			return -1
		endif
	else
		"If file doesnt exist - create one
		call Changer_FileCreate(fileName)
	endif


	let g:Changer_autoCmdNo[1]=1
	"And - change to it.
	let mode=Changer_ViewModeGet()
		"Save viewmode
	"Set normal Viewmode
	if !a:dontLeaveBuffer
		call Changer_ViewModeSet(1)
	endif

	"Go to previous window
	wincmd p
	try
		exe 'edit '.fileName
	catch /^Vim\%((\a\+)\)\=:E37/
		"Let's try to ask user
		if confirm("File ".bufname("%")." is being replaced. Save changes to it?","&yes\n&no")==1
			w
			exe 'edit '.fileName
		else
			exe 'edit! '.fileName
		endif
	finally
		wincmd p
		"Now we're back at list buffer and we can perform some operations
		if a:dontLeaveBuffer
			"setting viewmode back
			call Changer_ViewModeSet(mode)
		else
			wincmd p
		endif
	endtry
	let g:Changer_autoCmdNo[1]=0
endfunction

function! Changer_Absolutize(string)
	let splitted=matchlist(a:string,'\v^.*/(/.*)')
	if len(splitted)<2
		return a:string
	else
		return splitted[1]
	endif
endfunction

function! Changer_TeeExecute()
	let err=!Changer_Significant(getline("."),["exec"])
	if err==-1
		"It's unsignificant because line is unsignificant
		return -1
	endif

	let result=system(Changer_Absolutize(Changer_PathExtract(bufname("%"))."./".Changer_FullNameGet(line(".")))." ".Changer_ArgsGet(getline(".")))
	wincmd p
	let resultList = split(result,'\n')
	call append(line("$"),resultList)
	norm G
endfunction

function! Changer_ShellExecute()
	let err=!Changer_Significant(getline("."),["exec"])
	if err==-1
		"It's unsignificant because line is unsignificant
		return -1
	endif

	echomsg Changer_ArgsGet(getline("."))
	exe "!".Changer_Absolutize(Changer_PathExtract(bufname("%"))."./".Changer_FullNameGet(line(".")))." ".Changer_ArgsGet(getline("."))
endfunction

let &cpo = s:save_cpo
