"Vim config file with pavel shved's personal settings
set nocompatible              " be iMproved, required

" Installation:
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin '907th/vim-auto-save'
Plugin 'scrooloose/nerdcommenter'
Plugin 'davidhalter/jedi-vim'
Plugin 'iamcco/markdown-preview.vim'
Plugin 'derekwyatt/vim-fswitch'

" go go plugins!
call vundle#end()

" Plug system
call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'yegappan/mru'
call plug#end()


filetype plugin indent on
syntax on
set tabstop=2
set shiftwidth=2
set expandtab
set ruler

let mapleader = ","
map <Leader>s :FSHere<CR>

hi Comment      ctermfg=DarkBlue 
" Visual selection is buggy on my machine for some reason...
hi Visual ctermbg=111

" UTF-8
"set langmap=йцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕHГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ;qwertyuiop[]asdfghjkl;'zxcvbnm,.QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>

"Indenting
"set si
filetype indent plugin on
"set comments not jumping automatically to first line
inoremap # 9#

"Set Ctrl+B as 'Next file' action
nmap  :n<CR>

" paste-nopaste
set pastetoggle=<F3>

"Setting backspace in normal mode
nmap <BS> i<BS>

"Abbreviation section
iabbrev #i #include
iabbrev stds std::string
"Typos section
iabbrev cosnt const

"OCAML
"autocmd FileType ocaml nmap <F3> :exec 'echomsg (system("~/.vim/scripts/ocaml.annot.pl' bufname('%') (line2byte(line('.'))+col('.')-1) '"))'<RETURN> 

" Trailing spaces visualization
hi TrailingSpaces  ctermbg=DarkRed
match TrailingSpaces /\s\+$\| \+\ze\t/

se exrc

let $FZF_DEFAULT_COMMAND = "fdfind -L --type f"

" Configure FZF
map <C-P> :FZF<CR>
" - Popup window (anchored to the bottom of the current window)
"   my system doesn't have new enough vim for that
" let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.4, 'relative': v:true, 'yoffset': 1.0 } }
let g:fzf_layout = { 'down': '30%' }

command! FZFMruzzzzx call fzf#run({
\  'source':  v:oldfiles,
\  'sink':    'e',
\  'options': '-m -x +s',
\  'down':    '40%'})
map <C-F> :FZFMru<CR>
