# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTSIZE=10000
export HISTFILESIZE=100000

# Avoid duplicates
export HISTCONTROL=ignoredups:erasedups:ignoreboth

# When the shell exits, append to the history file instead of overwriting it
shopt -s histappend

# After each command, append to the history file but do not re-read, so that every terminal has its own
# history.
PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a;"

# Re-read the history to copy it from another terminal
alias hr="history -r"

