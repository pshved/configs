get_argocd_admin_secret () {
	kubectl -n argocd get secret argocd-initial-admin-secret -o json | jq -r '.data.password | @base64d'
}
start_argocd () {
	echo `get_argocd_admin_secret`
	local pod_name=$(kubectl get pods --namespace argocd -l "app.kubernetes.io/name=argocd-server" -o jsonpath="{.items[0].metadata.name}")
	kubectl -n argocd port-forward --address 0.0.0.0 $pod_name 8085:8080 "$@"
}

get_grafana_admin_secret () {
	kubectl -n grafana-loki get secret grafana -o json | jq -r '.data."admin-user" | @base64d'
	kubectl -n grafana-loki get secret grafana -o json | jq -r '.data."admin-password" | @base64d'
}
start_grafana () {
	echo `get_grafana_admin_secret`
	local pod_name=$(kubectl get pods --namespace grafana-loki -l "app.kubernetes.io/name=grafana" -o jsonpath="{.items[0].metadata.name}")
	kubectl -n grafana-loki port-forward $pod_name 8086:3000
}
