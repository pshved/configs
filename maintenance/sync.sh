#!/bin/bash

# Save
for f in $(find '(' -name .git -o -name maintenance ')' -prune -o -print ) ; do cp $HOME/${f##./} "$f" ; done
for f in .bashrc.d/ ./.vim/autoload/; do rsync -avr $HOME/${f##./} "$f" ; done
# Ensure clean status
git diff-index --quiet HEAD || { echo "Unclean directory, not syncing."; exit 1; }

# Copy the existing files (not no -u)
maintenance/load.sh
