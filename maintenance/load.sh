#!/bin/bash
rsync --files-from=<( find '(' -name .git -o -name maintenance ')' -prune -o -print ) -avp . $HOME/
